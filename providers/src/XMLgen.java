import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import provider.PK;

public class XMLgen {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JAXBContext context;
		try {
			context = JAXBContext.newInstance(SampleInfObj.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			PK pk = new PK();
			pk.add("ka", "va");
			pk.add("kb", "vb");

			SampleInfObj sio = new SampleInfObj();
			sio.setPK(pk);
			m.marshal(sio, System.out);

			Writer w = null;
			try {
				w = new FileWriter("infos.xml");
				m.marshal(sio, w);
			} catch (IOException e) {
				try {
					w.close();
				} catch (Exception ec) {

					ec.printStackTrace();
				}
			}

		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

}
