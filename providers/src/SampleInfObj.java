import java.security.SecureRandom;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import provider.InformationObject;

@XmlRootElement
public class SampleInfObj extends InformationObject {

	@XmlElement
	private final String f;
	
	public SampleInfObj() {
		super();
		f = Integer.toHexString(new SecureRandom().nextInt());
	}

}
