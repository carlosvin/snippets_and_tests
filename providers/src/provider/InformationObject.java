package provider;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public abstract class InformationObject {
	
	@XmlElement
	private PK pk;
	
	public InformationObject() {
		pk = null;
	}
	
	public void setPK(PK peka){
		pk = peka;
	}
}
