package provider;

import java.util.Collection;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlType;

@XmlType
public class PK {
	
	public final HashMap<String, String> keys;
	
	public PK(){
		keys = new HashMap<String, String>();
	}
	
	public void add(String key, String value){
		keys.put(key, value);
	}
//	
//    @XmlElement(name = "pk")
//    public Collection<String> getValues(){
//		return keys.values();
//	}
  
}
